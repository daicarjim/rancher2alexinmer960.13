# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/helm" {
  version     = "1.3.2"
  constraints = "1.3.2"
  hashes = [
    "h1:qwL8ISnGPQJMsie6WmAweovActsjtVFyunLrk+CJ5yo=",
    "zh:0d8e1293cb99b61d3aefbab3f1c1e258e121d860312115e23b240e8acb92b855",
    "zh:17524fac3f1eb46901a27ecef0c054c8b5390994b2f0bd48746a5eb47e42fad5",
    "zh:74ff2d471fc934d0e65452f914248c23394937a9b4cfb560ce920d5c42568303",
    "zh:855255f4afe7b86d88744f5615b6b6a6172fa7fc28c24d8fb5838b715e3b8a97",
    "zh:8b3bb0f0e2e6908c3d41ee183451cb388a80cf576b0953ad3d1e06cb4de22842",
    "zh:b46d607cedfefc94460bbff8a9d46e50f7dce364dc4050a2df81357159e07f81",
    "zh:c9b9f3b0e6aaec7081230df257f89e00e522a3a283197126d88ae646551cde6e",
    "zh:ccb0b341351df79773367aa6d895b89647ceb9a75fff1c434ee480513515d112",
    "zh:e39f56174f61556f2937fe50035703346833555cb83f2a880e3a4d832262120e",
    "zh:f792f8b620551198807bed0752453ff0574b1b7b03ec9d9a580177b84049c700",
  ]
}

provider "registry.terraform.io/hashicorp/local" {
  version = "2.1.0"
  hashes = [
    "h1:EYZdckuGU3n6APs97nS2LxZm3dDtGqyM4qaIvsmac8o=",
    "zh:0f1ec65101fa35050978d483d6e8916664b7556800348456ff3d09454ac1eae2",
    "zh:36e42ac19f5d68467aacf07e6adcf83c7486f2e5b5f4339e9671f68525fc87ab",
    "zh:6db9db2a1819e77b1642ec3b5e95042b202aee8151a0256d289f2e141bf3ceb3",
    "zh:719dfd97bb9ddce99f7d741260b8ece2682b363735c764cac83303f02386075a",
    "zh:7598bb86e0378fd97eaa04638c1a4c75f960f62f69d3662e6d80ffa5a89847fe",
    "zh:ad0a188b52517fec9eca393f1e2c9daea362b33ae2eb38a857b6b09949a727c1",
    "zh:c46846c8df66a13fee6eff7dc5d528a7f868ae0dcf92d79deaac73cc297ed20c",
    "zh:dc1a20a2eec12095d04bf6da5321f535351a594a636912361db20eb2a707ccc4",
    "zh:e57ab4771a9d999401f6badd8b018558357d3cbdf3d33cc0c4f83e818ca8e94b",
    "zh:ebdcde208072b4b0f8d305ebf2bfdc62c926e0717599dcf8ec2fd8c5845031c3",
    "zh:ef34c52b68933bedd0868a13ccfd59ff1c820f299760b3c02e008dc95e2ece91",
  ]
}

provider "registry.terraform.io/hetznercloud/hcloud" {
  version     = "1.24.0"
  constraints = "1.24.0"
  hashes = [
    "h1:vvqGYZelldPCpFeaelRQ1Eif0Gv/Si1S5h9gKPlHhAU=",
    "zh:167864a19937ec8cb8a5c76ddbb0171dcc349b956783bad54868e334b5fdfce5",
    "zh:1ded8cd94cd70fd29385610bd6d7c8cbf4f5c24f41e343bb522f0f4e5c4a46d2",
    "zh:4b2dcab6c32d903c0a52354cd41062b83ccf29ae776c28425399b1d7033a8bc3",
    "zh:6602fffa0b5ea12bebef2cd9f362a51cc103f50aa0a1c2bd9913a08131d705b8",
    "zh:a3b365f71fbd9d45751f5caa13b5053d7e16ab6eee664fc5048199abfa57317a",
    "zh:b374b12a9cd7971983008b99567da888101e89085ccee35eced51494fd6764ca",
    "zh:bfb5e5ab097d412d74cdfe72fab509ef29f2fb651f2e80440e1f74c393eb3914",
    "zh:c23febaf02ad46a528adfa98011e8fa66dbd55b75971a31eeb2a54d797ffd2e0",
    "zh:cc608d2a967383cb951e8796f844321a80d7cae3797c455d4b9f3a9f585f51e0",
    "zh:d3922631a3ef57c130471dad91fc868b961460d6a7a193398e9ed646712a92f7",
    "zh:e68342eea16b34a69d3f615fdd27176c59430f93a65e9f9eb52b27827549ffba",
  ]
}

provider "registry.terraform.io/rancher/rancher2" {
  version     = "1.11.0"
  constraints = "1.11.0"
  hashes = [
    "h1:4Duo5SQyArfvEB0CTrCzSiT5hdny6rTuuYDcfVezJNM=",
    "zh:1c7464f4a8711a92a06e1d5deb3f3785223f924bdf645b6ebe42d4d3783d0fee",
    "zh:716e543ae2e817bb48b80f0c10e014215b58f2521c731b5bdac0177886aa4150",
    "zh:748512912c686d4f1c578d9db890f091953cb5818eaec91cf49df58050e7b0d9",
    "zh:95bc35405b562ada9b33fc39ee6e1668842a59c74bce9b5912779b65c3a58936",
    "zh:98d4f138bfffdd0f73233250b22aefd7255bfac973d7bbd7677ee4decbe55ce2",
    "zh:9d2e543000268efcabec16d11cbe3a6e605d9d2587588aa630ec81133f5d460a",
    "zh:afa56b1e34cbfd9eb26e6ef8bda27a875974da7524c7ca2bb5522c0d397b4a23",
    "zh:b296cd8bfeabc53d42328b3b621406349dde765f7d64157cb6df64507b29c931",
    "zh:b40e9b1ccf2f2b00295565453761b1baa489a9a9bf362932aa16c9d5ec4e94a9",
    "zh:d4124ce4fea3537f6401441d2b2f49c1d59c1efad06654f7b68691831924ecf6",
    "zh:db56682e90228868e3ceba1f394ccf9e2b5fc5ae57b73ba953b37b2bb9adc258",
  ]
}

provider "registry.terraform.io/rancher/rke" {
  version     = "1.1.7"
  constraints = "1.1.7"
  hashes = [
    "h1:ojfIqJYDMHQhVbWXKSKQPmCcNKV+xlObIdwYLI56cFI=",
    "zh:1163acbe12001291e60e0ccff5d40524ed2aa4df8c626348e74bec63f8a37aff",
    "zh:3aad664328330bda53e9b5788f3f8fb75ebf2fb818320c872ba9b65fca1497e5",
    "zh:3aed499c342213064525dc427e4b25dc1d892d5229252d8b0217262184aba8e1",
    "zh:4cf3f496323122427e1da7e8bc7d02f9f5c68660f7e8310180555d7a89d2c2f0",
    "zh:812cf750b1ad8c254b85069b644f2ab1859e53485a3c26160d0cac29091ca828",
    "zh:8c44a87540d9acd5078584e99aa8bd8088a8f1b55096f9ab4bf54c7c1949c6ce",
  ]
}
